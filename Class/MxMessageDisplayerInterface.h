//
//  Created by mxm on 18/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class MxMessageObject;

@protocol MxMessageDisplayerInterfaceDelegate

@required
    -(void)messageDisplayerMessageDismissed;

@end

@protocol MxMessageDisplayerInterface

@property(nonatomic, assign) NSObject <MxMessageDisplayerInterfaceDelegate> *delegate;

-(void)displayError:(NSError *)error;

- (void)displayMessage:(MxMessageObject *)messageObject;

@end