//
//  Created by mlecomte on 06/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "MxMessageDisplayerInterface.h"


@class MxMessageQueue;
@class MxMessageObject;
@protocol MxMessageDisplayerInterface;


@interface MxMessageManager : NSObject <MxMessageDisplayerInterfaceDelegate> {
    MxMessageQueue *_messageQueue;
    BOOL _aMessageIsDisplayed;
    id<MxMessageDisplayerInterface> _messageDisplayer;
}

+ (void)initializeWithMessageDisplayer:(id <MxMessageDisplayerInterface>)messageDisplayer;

+ (MxMessageManager *)sharedManager;

- (void)showError:(NSError *)error;

-(void)showMessage:(MxMessageObject *)theMessageObject;

@end