//
//  Created by mlecomte on 06/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class MxMessageObject;


@interface MxMessageQueueObject : NSObject {

@private
    NSString *_identifiant;
    NSObject *_messageObject;
    NSString *_title;
}

- (MxMessageQueueObject *)initWithMessageObject:(MxMessageObject *)messageObject;

-(id)initWithError:(NSError *)error;

- (BOOL)isSameAs:(MxMessageQueueObject *)object;

- (NSString *)identifiant;

- (NSString *)title;

- (NSObject *)internalObject;

@end