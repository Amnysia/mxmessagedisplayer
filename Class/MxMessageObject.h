//
//  Created by mlecomte on 17/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface MxMessageObject : NSObject {

@private
    NSString *_message;
    NSString *_title;
}

@property(nonatomic, retain) NSString *message;
@property(nonatomic, retain) NSString *title;

- (id)initWithTitle:(NSString *)title andMessage:(NSString *)message;
@end