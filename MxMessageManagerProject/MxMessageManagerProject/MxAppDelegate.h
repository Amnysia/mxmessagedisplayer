//
//  MxAppDelegate.h
//  MxMessageManager
//
//  Created by Maxime Lecomte on 01/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MxViewController;

@interface MxAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MxViewController *viewController;

@end