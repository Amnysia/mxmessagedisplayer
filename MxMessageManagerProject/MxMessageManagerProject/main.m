//
//  main.m
//  MxMessageManager
//
//  Created by Maxime Lecomte on 01/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MxAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MxAppDelegate class]));
    }

}