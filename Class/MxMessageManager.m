//
//  Created by mlecomte on 06/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "MxMessageManager.h"
#import "MxMessageQueue.h"
#import "MxMessageQueueObject.h"
#import "MxMessageObject.h"

static MxMessageManager *_sharedinstance;

@interface MxMessageManager ()

@property(nonatomic, retain) MxMessageQueue *messageQueue;

@property(nonatomic) BOOL aMessageIsDisplayed;

- (void)displayMessageObject:(MxMessageQueueObject *)messageObject;

- (void)showFirstMessage;

@property(nonatomic, retain) id<MxMessageDisplayerInterface> messageDisplayer;


@end


@implementation MxMessageManager
@synthesize messageQueue = _messageQueue;
@synthesize aMessageIsDisplayed = _aMessageIsDisplayed;
@synthesize messageDisplayer = _messageDisplayer;


- (void)dealloc {
    [_messageQueue release];
    [_messageDisplayer release];
    [super dealloc];
}

+(void)initializeWithMessageDisplayer:(id<MxMessageDisplayerInterface>)messageDisplayer {
    [_sharedinstance release];
    _sharedinstance = [[MxMessageManager alloc] init];
    _sharedinstance.messageQueue = [[[MxMessageQueue alloc] init] autorelease];
    _sharedinstance.aMessageIsDisplayed = NO;
    _sharedinstance.messageDisplayer = messageDisplayer;
    _sharedinstance.messageDisplayer.delegate = _sharedinstance;
}

+ (MxMessageManager *)sharedManager {
    if (_sharedinstance == nil) {
        [NSException raise:@"Please initialize the BTVMessageManager before using it. // +(void)initializeWithMessageDisplayer:(id<BTVMessageDisplayerInterface>)messageDisplayer ) \\. It need a BTVMessageDisplayer to display correctly the message" format:@"Please initialize the BTVMessageManager before using it. It need a BTVMessageDisplayer to display correctly the message"];
    }
    return _sharedinstance;
}

- (void)showError:(NSError *)error {
    MxMessageQueueObject *messageObject = [[MxMessageQueueObject alloc] initWithError:error];
    [self displayMessageObject:messageObject];
    [messageObject release];
    return;
}

- (void)showMessage:(MxMessageObject *)theMessageObject {
    MxMessageQueueObject *messageQueueObject = [[MxMessageQueueObject alloc] initWithMessageObject:theMessageObject];
    [self displayMessageObject:messageQueueObject];
    [messageQueueObject release];
}


- (void)displayMessageObject:(MxMessageQueueObject *)messageObject {
    if (![self.messageQueue existInQueue:messageObject]) {
        [self.messageQueue addObject:messageObject];
        if (!_aMessageIsDisplayed) {
            [self showFirstMessage];
        }
    }
    return;
}

- (void)showFirstMessage {
    _aMessageIsDisplayed = YES;
    MxMessageQueueObject *messageObject = [self.messageQueue pop];
    if ([[messageObject internalObject] isKindOfClass:[NSError class]]) {
        [self.messageDisplayer displayError:(NSError *) [messageObject internalObject]];
    }
    else if ([[messageObject internalObject] isKindOfClass:[MxMessageObject class]]) {
        [self.messageDisplayer displayMessage:(MxMessageObject *) [messageObject internalObject]];
    }
    else {
        [NSException raise:@"The BTVMessageQueueObject is an unsupported type, please check your code" format:@"The WAMessageQueueObject is an unsupported type, please check your code"];
    }
}

- (void)messageDisplayerMessageDismissed {
    _aMessageIsDisplayed = NO;
    if ([self.messageQueue hasStillMessage]) {
        [self performSelector:@selector(showFirstMessage) withObject:nil afterDelay:0.5];
    }
}

@end