//
//  Created by mlecomte on 06/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@class MxMessageQueueObject;


@interface MxMessageQueue : NSObject {

    NSMutableArray *_queue;
}

-(void)addObject:(MxMessageQueueObject *)object;

-(MxMessageQueueObject *)pop;

-(BOOL)existInQueue:(MxMessageQueueObject *)object;

- (BOOL)hasStillMessage;
@end