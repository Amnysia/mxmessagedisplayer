//
//  Created by mlecomte on 06/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "MxMessageQueueObject.h"
#import "MxMessageObject.h"


@implementation MxMessageQueueObject

- (MxMessageQueueObject *)initWithMessageObject:(MxMessageObject *)messageObject {
    self = [super init];
    if (self) {
        _messageObject = messageObject;
        _identifiant = messageObject.message;
    }
    return self;
}

- (id)initWithError:(NSError *)error {
    self = [super init];
    if (self) {
        _messageObject = [error retain];
        NSString *errorDomain = [error domain];
        _identifiant = [errorDomain retain];
    }
    return self;
}

- (BOOL)isSameAs:(MxMessageQueueObject *)object {
    BOOL areSame = NO;
    if ([[self identifiant] isEqualToString:[object identifiant]]) {
        areSame = YES;
    }
    return areSame;
}

- (NSString *)identifiant {
    return _identifiant;
}

- (NSString *)title {
    return _title;
}

- (NSObject *)internalObject {
    return _messageObject;
}


- (void)dealloc {
    [_identifiant release];
    [_messageObject release];
    [_title release];
    [super dealloc];
}

@end