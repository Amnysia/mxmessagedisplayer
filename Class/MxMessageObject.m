//
//  Created by mlecomte on 17/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "MxMessageObject.h"


@implementation MxMessageObject
@synthesize message = _message;
@synthesize title = _title;


- (id)initWithTitle:(NSString *)title andMessage:(NSString *)message {
    self = [super init];
    if (self) {
        self.title = title;
        self.message = message;
    }
    return  self;
}

- (void)dealloc {
    [_message release];
    [_title release];
    [super dealloc];
}
@end