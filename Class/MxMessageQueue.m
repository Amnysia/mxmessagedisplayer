//
//  Created by mlecomte on 06/01/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "MxMessageQueue.h"
#import "MxMessageQueueObject.h"


@implementation MxMessageQueue

- (void)dealloc {
    [_queue release];
    [super dealloc];
}

-(id)init {
    self = [super init];
    if (self) {
        _queue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addObject:(MxMessageQueueObject *)object {
    [_queue addObject:object];
}

- (MxMessageQueueObject *)pop {
    MxMessageQueueObject *object = [[_queue objectAtIndex:0] retain];
    [_queue removeObjectAtIndex:0];
    return [object autorelease];
}

- (BOOL)existInQueue:(MxMessageQueueObject *)object {
    BOOL existInQueue = NO;
    for (MxMessageQueueObject *objectInQueue in _queue) {
        if ([objectInQueue isSameAs:object]) {
            existInQueue = YES;
            break;
        }
    }
    return existInQueue;
}

- (BOOL)hasStillMessage {
   return (_queue.count > 0);
}
@end